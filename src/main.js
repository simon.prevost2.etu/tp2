import data from './data.js';
import Component from './components/Components.js';
import Img from './components/Img.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';

const title = new Component('h1', null, ['La', ' ', 'carte']);

const img = new Img(
	'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
);

const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
	'Regina',
]);

const pizza = data[0];
const pizzaThumbnail = new PizzaThumbnail(pizza);

document.querySelector('.pageTitle').innerHTML = title.render();
//document.querySelector('.pageContent').innerHTML = img.render();
//document.querySelector('.pageContent').innerHTML = c.render();
document.querySelector('.pageContent').innerHTML = pizzaThumbnail.render();

console.log(title.render(), '\n', img.render(), '\n', c.render());
