//import Img from './Img.js';

export default class Component {
	tagname;
	children;
	attribute;
	constructor(tagname, attribute, children) {
		this.tagname = tagname;
		this.attribute = attribute;
		this.children = children;
	}
	renderChildren() {
		if (!this.children) {
			// Si pas de Children
			return '';
		}
		let res = '';
		if (this.children instanceof Array) {
			// Si Array
			for (let index = 0; index < this.children.length; index++) {
				res = res + this.children[index];
			}
			return res;
		} else if (this.children instanceof Component) {
			// Si Component
			return this.children.render();
		} else {
			// Si String
			res = `${this.children}`;
		}
		return res;
	}
	renderAttribute() {
		if (!this.attribute) {
			return `<${this.tagname}>`;
		} else if (!this.children) {
			return `<${this.tagname} ${this.attribute.name}="${this.attribute.value}"/>`; //balise auto fermante comme img
		} else {
			return `<${this.tagname} ${this.attribute.name}="${this.attribute.value}">`;
		}
	}
	render() {
		if (!this.children) {
			return this.renderAttribute();
		}
		return `${this.renderAttribute()}${this.renderChildren()}</${
			this.tagname
		}>`;
	}
}
