import Component from './Components.js';

export default class Img extends Component {
	constructor(img) {
		super('img', { name: 'src', value: img });
	}
}
